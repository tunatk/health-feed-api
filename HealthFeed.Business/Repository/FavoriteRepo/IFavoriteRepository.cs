﻿using HealthFeed.Business.Repository.PostRepo;
using HealthFeed.Entity.Models;
using System.Collections.Generic;

namespace HealthFeed.Business.Repository.FavoriteRepo
{
    public interface IFavoriteRepository
    {
        void AddFavori(Favorite favorite);
        bool ExistsFavorite(int userId, int postId);
        void DeleteFavorite(int userId, int postId);
        List<Favorite> GetAllFavorites(int UserId);
    }
}
