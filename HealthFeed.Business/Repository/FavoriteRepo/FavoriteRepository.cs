﻿using HealthFeed.Business.Repository.PostRepo;
using HealthFeed.DataAccess;
using HealthFeed.Entity.Models;
using System.Collections.Generic;
using System.Linq;

namespace HealthFeed.Business.Repository.FavoriteRepo
{
    public class FavoriteRepository : IFavoriteRepository
    {

        DataContext dataContext;
        public FavoriteRepository(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }


        public void AddFavori(Favorite favorite)
        {
            dataContext.Favorites.Add(favorite);
            dataContext.SaveChanges();
        }

        public void DeleteFavorite(int userId, int postId)
        {
            var fav = dataContext.Favorites.FirstOrDefault(x => x.UserId == userId && x.PostID == postId);
            if (fav != null)
            {
                dataContext.Favorites.Remove(fav);
                dataContext.SaveChanges();
            }
        }

        public bool ExistsFavorite(int userId, int postId)
        {
            var fav = dataContext.Favorites.FirstOrDefault(x => x.UserId == userId && x.PostID == postId);
            if (fav != null)
                return true;
            else
                return false;
        }

        public List<Favorite> GetAllFavorites(int userId)
        {
            return dataContext.Favorites.Where(x => x.UserId == userId).ToList();
        }

    }
}
