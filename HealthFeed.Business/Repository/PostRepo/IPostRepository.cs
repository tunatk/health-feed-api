﻿using HealthFeed.Entity.Models;
using System.Collections.Generic;
namespace HealthFeed.Business.Repository.PostRepo
{
    public interface IPostRepository
    {
        List<Post> GetAllPost();
        Post PostDetail(int Id);
        List<Post> FirstTwenty();
        List<Post> GetPostsByCategoryId(int CategoryId);
        void ChangePostStatus(int Id);
        void Update(Post editPost, int Id);
        void Delete(int Id);
        void Add(Post post);
        List<Post> GetPostsbyFavoriteUserId(int UserId);
    }
}
