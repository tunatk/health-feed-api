﻿using HealthFeed.DataAccess;
using HealthFeed.Entity.Models;
using System.Collections.Generic;
using System.Linq;

namespace HealthFeed.Business.Repository.PostRepo
{
    public class PostRepository : IPostRepository
    {
        #region DataContextAndConstruction
        DataContext dataContext;
        public PostRepository(DataContext _dataContext) => dataContext = _dataContext;
        #endregion
        #region Add
        public void Add(Post post)
        {
            dataContext.Posts.Add(post);
            dataContext.SaveChanges();
        }
        #endregion
        #region Delete
        public void Delete(int Id)
        {
            var s = dataContext.Posts.Find(Id);
            dataContext.Remove(s);
            dataContext.SaveChanges();
        }
        #endregion
        #region Update
        public void Update(Post editPost, int Id)
        {
            var post = dataContext.Posts.Find(Id);
            post.CategoryId = editPost.CategoryId;
            post.Title = editPost.Title;
            post.Description = editPost.Description;
            post.ImageUrl = editPost.ImageUrl;
            post.PubDate = editPost.PubDate;
            post.UrlParserId = editPost.UrlParserId;
            post.IsActive = editPost.IsActive;

            dataContext.SaveChanges();
        }
        #endregion
        #region FirtTwenty
        public List<Post> FirstTwenty() => dataContext.Posts.Where(x => x.IsActive == true).OrderByDescending(x => x.PubDate).Take(20).ToList();
        #endregion
        #region PostDetail
        public Post PostDetail(int Id) => dataContext.Posts.Where(x => x.Id == Id).FirstOrDefault();
        #endregion
        #region GetAllPost
        public List<Post> GetAllPost()
        {
            return dataContext.Posts.Where(x => x.IsActive == true).OrderByDescending(x => x.PubDate).ToList();
        }
        #endregion
        #region ChangePostStatus
        public void ChangePostStatus(int Id)
        {
            var data = dataContext.Posts.Where(x => x.Id == Id).FirstOrDefault();
            if (data.IsActive == true)
            {
                data.IsActive = false;
                dataContext.SaveChanges();
            }
            else
            {
                data.IsActive = true;
                dataContext.SaveChanges();
            }
        }
        #endregion
        #region GetPostsByCategoryId
        public List<Post> GetPostsByCategoryId(int CategoryId) => dataContext.Posts.Where(x => x.CategoryId == CategoryId).OrderByDescending(z => z.PubDate).ToList();
        #endregion
        public List<Post> GetPostsbyFavoriteUserId(int UserId)
        {
            var User = dataContext.Favorites.Where(x => x.UserId == UserId).ToList();
            List<Post> posts = new List<Post>();
            foreach (var item in User)
            {
                posts.Add(dataContext.Posts.Where(x=>x.Id==item.PostID).FirstOrDefault());
            }
            return posts;
        }

    }
}
