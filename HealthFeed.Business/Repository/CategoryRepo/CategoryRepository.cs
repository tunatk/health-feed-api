﻿using HealthFeed.DataAccess;
using HealthFeed.Entity.Models;
using System.Collections.Generic;
using System.Linq;

namespace HealthFeed.Business.Repository.CategoryRepo
{
    public class CategoryRepository : ICategoryRepository
    {
        #region DataContextAndConstruction
        DataContext dataContext;
        public CategoryRepository(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }
        #endregion

        #region Add
        public void Add(Category category)
        {
            var s = dataContext.Categories.ToList().Where(x => x.Name.ToUpper() == category.Name.ToUpper()).FirstOrDefault();
            if (s == null)
            {
                dataContext.Categories.Add(category);
                dataContext.SaveChanges();
            }
        }
        #endregion

        #region Delete
        public void Delete(int Id)
        {
            var Data = dataContext.Categories.Find(Id);
            dataContext.Remove(Data);
            dataContext.SaveChanges();
        }
        #endregion

        #region Update
        public void Update(Category categoryEdit, int Id)
        {
            var oCategory = dataContext.Categories.Find(Id);
            if (oCategory != null)
            {
                oCategory.Name = categoryEdit.Name;
                dataContext.SaveChanges();
            }
        }
        #endregion

        #region GetAllCategorys
        public List<Category> GetAllCategories()
        {
            return dataContext.Categories.ToList();
        }
        #endregion

    }
}
