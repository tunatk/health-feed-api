﻿using HealthFeed.Entity.Models;
using System.Collections.Generic;

namespace HealthFeed.Business.Repository.CategoryRepo
{
    public interface ICategoryRepository
    {
        void Delete(int Id);
        List<Category> GetAllCategories();
        void Update(Category category, int Id);
        void Add(Category category);
    }
}
