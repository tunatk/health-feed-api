﻿namespace HealthFeed.Business.Repository.AdminRepo
{
    public interface IAdminRepository
    {
        bool Login(string email, string password);
    }
}
