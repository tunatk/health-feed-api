﻿using HealthFeed.DataAccess;
using System.Linq;

namespace HealthFeed.Business.Repository.AdminRepo
{
    public class AdminRepository : IAdminRepository
    {
        DataContext context;
        public AdminRepository(DataContext _context)
        {
            context = _context;
        }
        public bool Login(string email, string password)
        {
            var realUser = context.AdminUsers.ToList().Exists(x => x.Email == email && x.Password == password);
            if (realUser)
                return true;
            else
                return false;
        }
    }
}
