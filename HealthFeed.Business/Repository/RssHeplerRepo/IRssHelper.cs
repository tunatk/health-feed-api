﻿using HealthFeed.Entity.Models;
using System.Collections.Generic;

namespace HealthFeed.Business.Repository.RssHeplerRepo
{
    public interface IRssHelper
    {
        List<Post> GetPosts();
        List<UrlParser> GetUrlParsers();
        void Edit(UrlParser post, int Id);
        void Delete(int Id);
        void Add(UrlParser parser);
    }
}
