﻿using HealthFeed.DataAccess;
using HealthFeed.Entity.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace HealthFeed.Business.Repository.RssHeplerRepo
{
    public class RssHelperRepository : IRssHelper
    {
        DataContext datacontext;
        public static int Ncount;
        public RssHelperRepository(DataContext _context)
        {
            datacontext = _context;
        }
        #region New Post Service
        public List<Post> GetPosts()
        {
            WebClient client = new WebClient();
            client.Encoding = System.Text.Encoding.UTF8;
            List<Post> posts = new List<Post>();
            XmlDocument document = new XmlDocument();

            bool ForValue = false;
            foreach (var item in datacontext.UrlParsers)
            {
                document.LoadXml(client.DownloadString(item.Url));
                if (document.DocumentElement.Name == "rdf:RDF")
                {
                    document.LoadXml(RDFToRssConverter(document));
                    ForValue = true;
                }
                XmlNamespaceManager xmlNamespace = new XmlNamespaceManager(document.NameTable);
                xmlNamespace.AddNamespace(item.ImageTag, "http://search.yahoo.com/mrss/");
                XmlNodeList nodeList = document.SelectNodes(item.RssRoad);
                foreach (XmlNode node in nodeList)
                {
                    posts.Add(new Post
                    {
                        CategoryId = 1,
                        UrlParserId = item.Id,
                        Title = node.SelectSingleNode(item.Title).InnerText,
                        Description = node.SelectSingleNode(item.Description).InnerText,
                        PubDate = ForValue == true ? Convert.ToDateTime(node.SelectSingleNode(item.PubDate).InnerText) : TimeCalc(node.SelectSingleNode(item.PubDate).InnerText),
                        ImageUrl = node.SelectSingleNode("enclosure") != null ? node.SelectSingleNode("enclosure").Attributes["url"].InnerText : node.SelectSingleNode("image") != null ? node.SelectSingleNode("image").Attributes["url"].InnerText : null,
                    });
                }
            }

            foreach (var item in posts)
            {
                if (!datacontext.Posts.ToList().Exists(x => x.Title == item.Title && x.Description == item.Description))
                {
                    datacontext.Posts.Add(item);
                    Ncount++;
                }
                datacontext.SaveChanges();
            }
            return posts;
        }
        private DateTime TimeCalc(string time)
        {
            int firstIndex = time.Split(" ").Length;
            return firstIndex > 0 ? DateTime.Parse(time.Replace($" {time.Split(" ")[firstIndex - 1]}", ""), CultureInfo.InvariantCulture.DateTimeFormat) : DateTime.Now;
        }

        public string RDFToRssConverter(XmlDocument rdfDoc)
        {

            System.Net.WebClient download_Obj = new System.Net.WebClient();
            UTF8Encoding utf = new UTF8Encoding();

            MatchCollection mc = null;
            Match m = null;
            Match m1 = null;

            string title = string.Empty;
            string link = string.Empty;
            string description = string.Empty;
            string author = string.Empty;
            string pubDate = string.Empty;
            string rssImage = string.Empty;

            RegexOptions ro = new RegexOptions();

            ro = ro | RegexOptions.IgnoreCase;
            ro = ro | RegexOptions.Multiline;

            string content = rdfDoc.InnerXml.ToString();

            content = content.Replace("\n", " ");
            content = content.Replace("\r", " ");

            const string rssVersion = "2.0";
            const string rssLanguage = "en-US";

            string rssGenerator = "RDFFeedConverter";
            MemoryStream memoryStream = new MemoryStream();
            XmlTextWriter xmlWriter = new XmlTextWriter(memoryStream, null);
            xmlWriter.Formatting = Formatting.Indented;
            Regex r = new Regex(@"<channel.*</channel>", ro);

            m = r.Match(content);
            r = new Regex(@"<title>.*</title>", ro);
            m1 = r.Match(m.ToString());
            string feedTitle = m1.ToString().Substring(
               m1.ToString().IndexOf(">") + 1, (
               m1.ToString().IndexOf("</") -
               m1.ToString().IndexOf(">") - 1));

            //string feedTitle = m1.ToString();
            r = new Regex(@"<link>.*</link>", ro);
            m1 = r.Match(m.ToString());
            string feedLink = m1.ToString().Substring(m1.ToString().IndexOf(">") + 1,
              (m1.ToString().IndexOf("</") - m1.ToString().IndexOf(">") - 1));

            //string feedLink = m1.ToString(); 
            r = new Regex(@"<description>.*</description>", ro);
            m1 = r.Match(m.ToString());
            string rssDescription = m1.ToString().Substring(m1.ToString().IndexOf(">") + 1,
              (m1.ToString().IndexOf("</") - m1.ToString().IndexOf(">") - 1));

            r = new Regex(@"<image>.*</image>", ro);
            m1 = r.Match(m.ToString());
            if (!string.IsNullOrEmpty(m1.ToString()))
            {
                rssImage = m1.ToString().Substring(m1.ToString().IndexOf(">") + 1,
                  (m1.ToString().IndexOf("</") - m1.ToString().IndexOf(">") - 1));
            }
            //string rssDescription = m1.ToString(); 
            xmlWriter.WriteStartElement("rss");
            xmlWriter.WriteAttributeString("version", rssVersion);
            xmlWriter.WriteStartElement("channel");
            xmlWriter.WriteElementString("title", feedTitle);
            xmlWriter.WriteElementString("image", rssImage);
            xmlWriter.WriteElementString("link", feedLink);
            xmlWriter.WriteElementString("description", rssDescription);
            xmlWriter.WriteElementString("language", rssLanguage);
            xmlWriter.WriteElementString("generator", rssGenerator);

            int index = content.IndexOf("</channel>");
            index += 10;

            content = content.Substring(index);

            int beginIndex = 0;
            int endIndex = 0;
            string itemContent = "";
            string str = "";

            r = new Regex(@"<item", ro);

            mc = r.Matches(content);

            for (int i = 0; i < mc.Count; i++)
            {
                beginIndex = mc[i].Index;
                if (i == mc.Count - 1)
                    endIndex = content.Length;
                else
                    endIndex = mc[i + 1].Index;
                itemContent = content.Substring(beginIndex, (endIndex - beginIndex));

                r = new Regex(@"<title>.*</title>", ro);  // * </channel>

                m1 = null;
                m1 = r.Match(itemContent);

                if (m1.Length != 0)
                {
                    //<![CDATA[
                    str = m1.ToString().Substring(m1.ToString().IndexOf(">") + 1,
                      (m1.ToString().IndexOf("</") - m1.ToString().IndexOf(">") - 1));
                    str = str.Trim();
                    if (str.StartsWith("<![CDATA["))
                        str = str.Substring(9, str.Length - 3 - 9);
                    title = str;
                }

                r = new Regex(@"<link>.*</link>", ro);
                m1 = r.Match(itemContent);

                if (m1.Length != 0)
                {
                    //<![CDATA[
                    str = m1.ToString().Substring(m1.ToString().IndexOf(">") + 1,
                      (m1.ToString().IndexOf("</") - m1.ToString().IndexOf(">") - 1));
                    str = str.Trim();
                    if (str.StartsWith("<![CDATA["))
                        str = str.Substring(9, str.Length - 3 - 9);
                    link = str;
                }

                r = new Regex(@"<description>.*</description>", ro);
                m1 = null;
                m1 = r.Match(itemContent);

                if (m1.Length != 0)
                {
                    //<![CDATA[
                    str = m1.ToString().Substring(m1.ToString().IndexOf(">") + 1,
                      (m1.ToString().IndexOf("</") - m1.ToString().IndexOf(">") - 1));
                    str = str.Trim();
                    if (str.StartsWith("<![CDATA["))
                        str = str.Substring(9, str.Length - 3 - 9);
                    description = str;
                }

                //r = new Regex(@"publicationDate>.*publicationDate>",ro);  
                r = new Regex(@"Date>.*Date>", ro);
                m1 = null;
                m1 = r.Match(itemContent);

                if (m1.Length != 0)
                {
                    //<![CDATA[
                    str = m1.ToString().Substring(m1.ToString().IndexOf(">") + 1,
                      (m1.ToString().IndexOf("</") - m1.ToString().IndexOf(">") - 1));
                    str = str.Trim();
                    if (str.StartsWith("<![CDATA["))
                        str = str.Substring(9, str.Length - 3 - 9);
                    pubDate = str;
                }

                r = new Regex(@"creator>.*creator>", ro);
                m1 = null;
                m1 = r.Match(itemContent);

                if (m1.Length != 0)
                {
                    m1.ToString().IndexOf(">");
                    m1.ToString().IndexOf("</");

                    //<![CDATA[
                    str = m1.ToString().Substring(m1.ToString().IndexOf(">") + 1,
                                          (m1.ToString().IndexOf("</") - m1.ToString().IndexOf(">") - 1));
                    str = str.Trim();

                    if (str.StartsWith("<![CDATA["))
                        str = str.Substring(9, str.Length - 3 - 9);

                    author = str;
                }

                if (title.Length == 0)
                    title = "Not Specified";

                if (link.Length == 0)
                    link = "Not Specified";

                if (description.Length == 0)
                    description = " Not Specified";

                if (author.Length == 0)
                    author = "Not Specified";

                if (pubDate.Length == 0)
                    pubDate = "Not Specified";

                xmlWriter.WriteStartElement("item");
                xmlWriter.WriteElementString("title", title);
                xmlWriter.WriteElementString("link", link);
                xmlWriter.WriteElementString("pubDate",
                  Convert.ToDateTime(pubDate).ToString());
                xmlWriter.WriteElementString("author", author);
                xmlWriter.WriteElementString("description", description);
                xmlWriter.WriteEndElement();

                title = string.Empty;
                link = string.Empty;
                description = string.Empty;
                author = string.Empty;
                pubDate = string.Empty;
                rssImage = string.Empty;

            }

            xmlWriter.WriteEndElement();
            xmlWriter.Flush();
            xmlWriter.Close();

            string outStr = Encoding.UTF8.GetString(memoryStream.ToArray());

            XmlDocument retDoc = new XmlDocument();
            retDoc.LoadXml(outStr);
            memoryStream.Close();

            return outStr;

        }//

        #endregion

        public List<UrlParser> GetUrlParsers()
        {
            return datacontext.UrlParsers.ToList();
        }

        public void Edit(UrlParser post, int Id)
        {
            throw new NotImplementedException();
        }

        public void Delete(int Id)
        {
            throw new NotImplementedException();
        }

        public void Add(UrlParser parser)
        {
            datacontext.UrlParsers.Add(parser);
            datacontext.SaveChanges();
        }
    }
}
