﻿using HealthFeed.Entity.Models;

namespace HealthFeed.Business.Repository
{
    public interface IUserRepository
    {
        User Login(string email, string password);
        User Register(User user);
        bool UserExists(string email);
        User UpdateProfile(int Id, User user);
        User GetUserById(int Id);
        Branch GetBranchById(int Id);
    }
}
