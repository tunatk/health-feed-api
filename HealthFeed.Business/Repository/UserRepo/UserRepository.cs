﻿using HealthFeed.DataAccess;
using HealthFeed.Entity.Models;
using System.Linq;
namespace HealthFeed.Business.Repository
{
    public class UserRepository : IUserRepository
    {
        DataContext dataContext;
        public UserRepository(DataContext _dataContext)
        {
            dataContext = _dataContext;
        }

        public Branch GetBranchById(int Id)
        {
            Branch branch = new Branch();
            var _brancId = dataContext.Users.Find(Id).BranchId;
            var s = dataContext.Branches.Where(x => x.Id == _brancId).FirstOrDefault();
            branch.Id = s.Id;
            branch.Name = s.Name;
            return branch;
        }

        public User GetUserById(int Id)
        {
            return Id != 0 ? dataContext.Users.Find(Id) : null;
        }

        public User Login(string email, string password)
        {
            var user = dataContext.Users.FirstOrDefault(x => x.Email == email && x.Password == password);
            if (user == null)
                return null;
            else
                return user;
        }

        public User Register(User user)
        {
            if (dataContext.Users.FirstOrDefault(x => x.Email == user.Email) == null)
            {
                dataContext.Users.Add(user);
                dataContext.SaveChanges();
                return user;
            }
            else
                return null;
        }
        public User UpdateProfile(int Id, User user)
        {
            var userOld = dataContext.Users.Find(Id);
            userOld.Name = user.Name;
            userOld.Email = user.Email;
            userOld.Password = user.Password;
            userOld.LastName = user.LastName;
            userOld.BranchId = user.BranchId;
            userOld.Birthday = user.Birthday;
            userOld.PhoneNumber = user.PhoneNumber;
            userOld.ProfilePicture = user.ProfilePicture;
            dataContext.SaveChanges();
            return user;
        }

        public bool UserExists(string email)
        {
            return dataContext.Users.FirstOrDefault(x => x.Email == email) != null ? true : false;
        }
    }
}
