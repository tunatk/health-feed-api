﻿using HealthFeed.Entity.Models;
using Microsoft.EntityFrameworkCore;

namespace HealthFeed.DataAccess
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Favorite> Favorites { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<UrlParser> UrlParsers { get; set; }
        public DbSet<AdminUser> AdminUsers { get; set; }
    }
}
