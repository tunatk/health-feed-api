﻿using HealthFeed.DataAccess;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace HealthFeedCpanel.Controllers
{
    public class UserController : Controller
    {
        private DataContext context;
        public UserController(DataContext _context)
        {
            context = _context;
        }
        public IActionResult Index()
        {
            return View(context.Users.ToList());
        }



        public IActionResult UserHeader()
        {
            return PartialView();
        }
    }
}