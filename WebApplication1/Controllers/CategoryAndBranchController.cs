﻿using HealthFeed.DataAccess;
using HealthFeed.Entity.Models;
using HealthFeedCpanel.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace HealthFeedCpanel.Controllers
{
    public class CategoryAndBranchController : Controller
    {
        private DataContext _context;

        public CategoryAndBranchController(DataContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            ViewModel model = new ViewModel();
            model.Category = _context.Categories.ToList();
            model.Branch = _context.Branches.ToList();
            return View(model);
        }
        #region Category Create
        /// <summary>
        /// New Category
        /// </summary>
        /// <returns></returns>
        public IActionResult Create() => View();

        [HttpPost]
        public IActionResult Creating(Category category)
        {
            if (ModelState.IsValid)
            {
                _context.Categories.Add(category);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
        #endregion
        #region Branch Create  
        /// <summary>
        /// Create Branch
        /// </summary>
        /// <returns></returns>

        public IActionResult CreateBranch() => View();
        [HttpPost]
        public IActionResult CreatingBranch(Branch branch)
        {
            if (ModelState.IsValid)
            {
                _context.Branches.Add(branch);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
        #endregion
        #region Edit Category
        public IActionResult Edit(int? id)
        {
            if (id != null)
            {
                var category = _context.Categories.Find(id);
                if (category == null)
                    return NotFound();
                return View(category);
            }
            else
                return NotFound();
        }
        [HttpPost]
        public IActionResult EditCategory(Category category)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    Category category1 = new Category();

                    category1.Id = category.Id;
                    category1.Name = category.Name;
                    category1.IsActive = category.IsActive;
                    _context.Update(category1);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
        #endregion
        #region Edit Branch
        public IActionResult EditBranch(int? id)
        {
            if (id != null)
            {
                var branch = _context.Branches.Find(id);
                if (branch == null)
                    return NotFound();
                return View(branch);
            }
            else
                return NotFound();
        }
        [HttpPost]
        public IActionResult EditingBranch(Branch branch)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(branch);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
        #endregion

        [HttpPost("CategoryActive/{id}")]
        public IActionResult Active(int id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var db = _context.Categories.Find(id);
                    if (db.IsActive)
                        db.IsActive = false;
                    else
                        db.IsActive = true;
                    _context.Categories.Update(db);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
    }
}