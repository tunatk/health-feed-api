﻿using HealthFeed.Business.Repository.AdminRepo;
using HealthFeed.DataAccess;
using HealthFeed.Entity.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HealthFeedCpanel.Controllers
{
    public class AdminController : Controller
    {
        DataContext _context;
        public AdminController(DataContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }
        //  [HttpPost]
        //public ActionResult Index(string email, string password)
        //{
        //    var Rvalue = repository.Login(email, password);
        //    if (Rvalue)
        //    {

        //        return RedirectToAction("Index", "Home");
        //    }
        //    else
        //        ViewBag.success = false;
        //    return View();
        //}
        [HttpPost]
        public async Task<IActionResult> Index(AdminUser loginModel)
        {
            AdminRepository repository = new AdminRepository(_context);

            if (repository.Login(loginModel.Email, loginModel.Password))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, loginModel.Email)
                };

                var userIdentity = new ClaimsIdentity(claims, "login");

                ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                await HttpContext.SignInAsync(principal);


                //Just redirect to our index after logging in. 
                return RedirectToAction("Index", "Home");
            }
            else
                ViewBag.success = false;
            return View();
        }

        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();

            return RedirectToAction("Index");
        }
    }
}