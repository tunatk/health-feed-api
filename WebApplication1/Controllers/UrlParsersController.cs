﻿using HealthFeed.DataAccess;
using HealthFeed.Entity.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
namespace HealthFeedCpanel.Controllers
{
    public class UrlParsersController : Controller
    {
        private readonly DataContext _context;

        public UrlParsersController(DataContext context)
        {
            _context = context;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View(_context.UrlParsers.ToList());
        }

        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var UrlParsers = await _context.UrlParsers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (UrlParsers == null)
            {
                return NotFound();
            }

            return View(UrlParsers);
        }
        [HttpPost]
        public ActionResult Create()
        {
            return PartialView();
        }

        // POST: UrlParsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Creating([Bind("Id,Url,Title,Description,Image,ImageTag,RssRoad,PubDate")] UrlParser UrlParsers)
        {
                if (ModelState.IsValid)
                {
                    _context.Add(UrlParsers);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            return View(UrlParsers);
        }

        [HttpPost]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var UrlParsers = _context.UrlParsers.Find(id);
            if (UrlParsers == null)
            {
                return NotFound();
            }
            return PartialView(UrlParsers);
        }
        [Authorize]

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Editing(int id, [Bind("Id,Url,Title,Description,Image,ImageTag,RssRoad,PubDate")] UrlParser UrlParsers)
        {
            if (id != UrlParsers.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(UrlParsers);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UrlParsersExists(UrlParsers.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(UrlParsers);
        }

        [HttpPost]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                var UrlParsers = _context.UrlParsers.Find(id);
                _context.UrlParsers.Remove(UrlParsers);
                _context.SaveChanges();
                return Json(true);
            }

            catch (Exception)
            {
                return Json(false);
            }
        }

        private bool UrlParsersExists(int id)
        {
            return _context.UrlParsers.Any(e => e.Id == id);
        }
        public IActionResult UrlHeader()
        {
            return PartialView();
        }
    }
}
