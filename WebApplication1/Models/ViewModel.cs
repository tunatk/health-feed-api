﻿using HealthFeed.Entity.Models;
using System.Collections.Generic;

namespace HealthFeedCpanel.Models
{
    public class ViewModel
    {
        public List<Category> Category { get; set; }
        public List<Post> Post { get; set; }
        public List<Branch> Branch { get; set; }
    }
}
