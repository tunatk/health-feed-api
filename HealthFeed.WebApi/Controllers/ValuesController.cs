﻿using Microsoft.AspNetCore.Mvc;

namespace HealthFeed.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public string Get() => $"Is Working Go to Api Details /swagger";

    }

}
