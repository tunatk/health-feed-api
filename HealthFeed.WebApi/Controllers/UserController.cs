﻿using HealthFeed.Business.Repository;
using HealthFeed.Entity.Models;
using HealthFeed.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace HealthFeed.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        IUserRepository userRepository;
        IConfiguration configuration;
        public UserController(IUserRepository _userRepository, IConfiguration _configuration)
        {
            userRepository = _userRepository;
            configuration = _configuration;
        }

        [HttpPost("register")]
        public IActionResult Register([FromBody]User user)
        {
            userRepository.Register(user);
            return StatusCode(201);
        }
        [HttpPut("UpdateUser/{Id}")]
        public IActionResult UpdateUser([FromBody]User user, int Id)
        {
            userRepository.UpdateProfile(Id, user);
            return Ok(user);
        }
        [HttpPost("login")]
        public IActionResult Login([FromBody] UserLoginModel user)
        {
            if (ModelState.IsValid)
            {
                var user1 = userRepository.Login(user.Email, user.Password);
                if (user1 == null)
                    return Unauthorized();
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(configuration.GetSection("AppSettings:Token").Value);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[] {
                    new Claim(ClaimTypes.NameIdentifier,user1.Id.ToString()),
                    new Claim(ClaimTypes.Name,user1.Email),
                    }),
                    Expires = DateTime.UtcNow.AddDays(30),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);
                return Ok(tokenString);
            }
            return BadRequest();
        }
        [HttpGet("GetUserbyId/{Id}")]
        public IActionResult GetUserById(int Id)
        {
            return Ok(userRepository.GetUserById(Id));
        }
        [HttpGet("GetBranchById/{Id}")]
        public IActionResult GetBranchById(int Id)
        {
            return Ok(userRepository.GetBranchById(Id));
        }
    }
}