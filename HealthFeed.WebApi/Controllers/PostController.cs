﻿using HealthFeed.Business.Repository.FavoriteRepo;
using HealthFeed.Business.Repository.PostRepo;
using HealthFeed.Entity.Models;
using Microsoft.AspNetCore.Mvc;

namespace HealthFeed.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        #region IPostRepositoryAndConstruction
        IPostRepository postRepository;
        IFavoriteRepository favoriteRepository;
        public PostController(IPostRepository _postRepository, IFavoriteRepository _favoriteRepository)
        {
            postRepository = _postRepository;
            favoriteRepository = _favoriteRepository;
        }
        #endregion

        #region Add
        [HttpPost("Add")]
        public IActionResult Add([FromBody] Post post)
        {
            postRepository.Add(post);
            return Ok();
        }
        #endregion

        #region Delete
        [HttpDelete("Delete/{Id}")]
        public IActionResult Delete(int Id)
        {
            postRepository.Delete(Id);
            return Ok();
        }
        #endregion

        #region Update
        [HttpPost("Update/{Id}")]
        public IActionResult Update(Post post, int Id)
        {
            postRepository.Update(post, Id);
            return Ok();
        }
        #endregion

        #region FirtTwenty
        [HttpGet("FirstTwenty")]
        public IActionResult FirstTwenty() => Ok(postRepository.FirstTwenty());

        #endregion

        #region PostDetail
        [HttpGet("PostDetail/{Id}")]
        public IActionResult PostDetail(int Id)
        {
            var Post = postRepository.PostDetail(Id);
            if (Post != null)
                return Ok(Post);
            else
                return StatusCode(404);
        }
        #endregion

        #region GetAllPosts
        [HttpGet("GetAllPosts")]
        public IActionResult GetAllPosts() => Ok(postRepository.GetAllPost());
        #endregion

        #region ChangePostsStatus
        [HttpPut("ChangePostsStatus/{Id}")]
        public IActionResult ChangePostsStatus(int Id)
        {
            postRepository.ChangePostStatus(Id);
            return Ok();
        }
        #endregion

        #region GetPostsByCategoryId
        [HttpGet("GetPostsByCategoryId/{Id}")]
        public IActionResult GetPostsByCategoryId(int Id) => Ok(postRepository.GetPostsByCategoryId(Id));
        #endregion

        #region AddFavorite
        [HttpPost("AddFavorite")]
        public IActionResult AddFavorite([FromBody] Favorite favorite)
        {
            favoriteRepository.AddFavori(favorite);
            return Ok();
        }
        #endregion

        #region DeleteFavorite
        [HttpDelete("DeleteFavorite/{userId}/{postId}")]
        public IActionResult DeleteFavorite(int userId, int postId)
        {
            favoriteRepository.DeleteFavorite(userId, postId);
            return Ok();
        }
        #endregion

        #region ExistsFavorite
        [HttpGet("ExistsFavorite/{userId}/{postId}")]
        public IActionResult ExistsFavorite(int userId, int postId) => Ok(favoriteRepository.ExistsFavorite(userId, postId));
        #endregion

        #region GetAllFavorites
        [HttpGet("GetAllFavorites/{userId}")]
        public IActionResult GetAllFavorites(int userId)=>Ok(favoriteRepository.GetAllFavorites(userId));
        #endregion

        #region GetPostsbyFavoriteUserId
        [HttpGet("GetPostsbyFavoriteUserId/{userId}")]
        public IActionResult GetPostsbyFavoriteUserId(int userId) => Ok(postRepository.GetPostsbyFavoriteUserId(userId));
        #endregion

    }

}