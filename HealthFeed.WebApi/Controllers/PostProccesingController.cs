﻿using HealthFeed.Business.Repository.RssHeplerRepo;
using HealthFeed.Entity.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HealthFeed.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostProccesingController : ControllerBase
    {
        IRssHelper rssHelper;
        public PostProccesingController(IRssHelper _rssHelper)
        {
            rssHelper = _rssHelper;
        }

        [HttpGet("StartPost")]
        public List<Post> StartPost()
        {
            return rssHelper.GetPosts();
        }
        [HttpPut("RssAdd")]
        public void RssAdd([FromBody] UrlParser urlParser)
        {
            rssHelper.Add(urlParser);
        }
    }
}