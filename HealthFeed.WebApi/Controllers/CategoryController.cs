﻿using HealthFeed.Business.Repository.CategoryRepo;
using HealthFeed.Entity.Models;
using Microsoft.AspNetCore.Mvc;

namespace HealthFeed.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        ICategoryRepository categoryRepository;
        public CategoryController(ICategoryRepository _categoryRepository)
        {
            categoryRepository = _categoryRepository;
        }

        #region Add
        [HttpPost("Add")]
        public IActionResult Add([FromBody]Category category)
        {
            categoryRepository.Add(category);
            return Ok();
        }
        #endregion

        #region Delete
        [HttpDelete("Delete/{Id}")]
        public IActionResult Delete(int Id)
        {
            categoryRepository.Delete(Id);
            return Ok();
        }
        #endregion

        #region Update
        [HttpPut("Update/{Id}")]
        public IActionResult Update(Category category, int Id)
        {
            categoryRepository.Update(category, Id);
            return Ok();
        }

        #endregion

        #region GetAllCategories
        [HttpGet("GetAllCategories")]
        public IActionResult GetAllCategories()
        {
            return Ok(categoryRepository.GetAllCategories());
        }
        #endregion


    }
}