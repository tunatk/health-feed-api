﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace HealthFeed.WebApi.RestartServices
{
    public class RequestCollectorService : HostedService
    {
        protected override async Task ExecuteAsync(CancellationToken cToken)
        {
            while (!cToken.IsCancellationRequested)
            {
                //Debug.WriteLine($"{DateTime.Now.ToString()} Çalışma zamanı taleplerini topluyorum.");
                await Task.Delay(TimeSpan.FromHours(11.30), cToken);
            }
        }
    }
}
