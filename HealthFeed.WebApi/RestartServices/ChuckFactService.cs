﻿using HealthFeed.Business.Repository.RssHeplerRepo;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace HealthFeed.WebApi.RestartServices
{
    public class ChuckFactService : HostedService
    {

        HttpClient restClient;
        //string icndbUrl = "https://localhost:44307/api/PostProccesing/startpost";
        string icndbUrl = "http://ting.ist/api/PostProccesing/startpost";
        public ChuckFactService()
        {
            restClient = new HttpClient();
        }
        protected override async Task ExecuteAsync(CancellationToken cToken)
        {
            while (!cToken.IsCancellationRequested)
            {
                var response = await restClient.GetAsync(icndbUrl, cToken);
                if (response.IsSuccessStatusCode)
                {
                    var fact = await response.Content.ReadAsStringAsync();
                    var text = ($"{DateTime.Now.ToString()}\n New Post Count:{ RssHelperRepository.Ncount} \n{fact}");

                    // Debug.WriteLine(text);//var LogLine = Path.Combine("ApiLogs");

                    //if (!Directory.Exists(LogLine))
                    //    Directory.CreateDirectory(LogLine);
                    //StreamWriter fs = File.CreateText($@"ApiLogs\{DateTime.Now.ToString()}.txt");
                    //fs.Write(text);
                }
                await Task.Delay(TimeSpan.FromHours(11), cToken);
            }
        }
    }
}
