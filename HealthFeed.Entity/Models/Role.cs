﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthFeed.Entity.Models
{
    public class Role
    {
        //public Role()
        //{
        //    Users = new List<User>();
        //}
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserRole> UserRoles { get; set; }

    }
}