﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthFeed.Entity.Models
{
    public class User
    {
        //public User()
        //{
        //    Roles = new List<Role>();
        //}
        [Key]
        public int Id { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string ProfilePicture { get; set; }
        public int BranchId { get; set; }
        public List<UserRole> Roles { get; set; }
        [Required]
        public DateTime Birthday { get; set; }
        public List<Favorite> Favorites { get; set; }


        public Branch Branch { get; set; }
    }
}
