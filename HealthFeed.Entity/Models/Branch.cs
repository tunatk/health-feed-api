﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthFeed.Entity.Models
{
    public class Branch
    {
        public Branch()
        {
            UserList = new List<User>();
        }
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> UserList { get; set; }

    }
}