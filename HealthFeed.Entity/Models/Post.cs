﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace HealthFeed.Entity.Models
{
    public class Post
    {
        public Post()
        {
            CreateDate = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public DateTime PubDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int UrlParserId { get; set; }
        public bool IsActive { get; set; }
        [NotMapped]
        public string PostTag
        {
            get
            {
                var s = Regex.Replace(Title, @"[\d-]", "").Replace(":", "").Replace("?", "").Replace(",", "").Replace(".", "").Replace("!", "").Replace("'", "").Replace("#", "").Replace("<", "").Replace("[", "").Replace("]", "").Replace(";", "");
                var w = s.Replace(" ", "");
                return w;
            }
            set
            {
            }
        }

        public List<Favorite> Favorites { get; set; }
        public Category Category { get; set; }
        public UrlParser UrlParser { get; set; }

    }
}