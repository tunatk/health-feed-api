﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HealthFeed.Entity.Models
{
    public class Favorite
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PostID { get; set; }

        public User User { get; set; }
        public Post Post { get; set; }

    }
}