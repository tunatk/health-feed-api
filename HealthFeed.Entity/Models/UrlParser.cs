﻿using System.ComponentModel.DataAnnotations;

namespace HealthFeed.Entity.Models
{
    public class UrlParser
    {
        [Key]
        public int Id { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string ImageTag { get; set; }
        public string RssRoad { get; set; }
        public string PubDate { get; set; }
    }
}